#include <stdio.h>
#include <math.h>

#define K 1.93
#define N 100

double x[N];
double y[N];

double sinc(double x)
{
	return sin(x) / x;
}

int main(void)
{
	for (int i = 1; i < N; i++)
	{
		x[i] = K * i;
		y[i] = sinc(x[i]);
	}
	// Je n'utilise pas CVI, donc pas de plotting
}