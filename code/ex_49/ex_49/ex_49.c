#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>

#include "perso.h"

#define SIZE 10

int tab[SIZE] = {159, 159, 160, 160, 160, 165, 180, 180, 190, 190};
int tab_fixed[SIZE];
int seen_values[SIZE];

int main(void)
{
	bool valid = true;
	int shift = 0;
	for (int i = 0; i <= SIZE; i++)
	{
		valid = true;
		for (int j = 0; j < SIZE; j++)
		{
			if (tab[i] == seen_values[j])
			{
				valid = false;
				shift++;
			}

		}
		if (valid == true)
		{
			seen_values[i - shift] = tab[i];
		}
	}
	return 0;
}