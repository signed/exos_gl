﻿#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

/*
    Exercices GL 04/22
    Copyright (C) 2022  Nicolas signed-log FORMICHELLA

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


void clear_screen(void)
{
// Checks for Windows
#ifdef _WIN32
    // ASCII Escape codes for screen clearing on Windows
    puts("\033[2J\033[1; 1H");
    printf("\x1b[%d;%df", 1, 1);
#else
    // ASCII Escape codes for screen clearing on POSIX
    puts("\e[1;1H\e[2J");
#endif
}

void delay(int number_of_seconds)
{
    // Converting time into milli_seconds
    int milli_seconds = 1000 * number_of_seconds;

    // Storing start time
    clock_t start_time = clock();

    // looping till required time is not achieved
    while (clock() < start_time + milli_seconds)
        ;
}

int inputi(unsigned char message)
{
    int in;
    fflush(stdin);
#ifdef _WIN32
    printf_s("%c : ", message);
    scanf_s("%2d", &in);
#else
    printf("%c : ", message);
    scanf("%2d", &in);
#endif
    fflush(stdin);
    return in;
}

void wait_for_enter(void)
{
#ifdef _WIN32
    printf_s("%s", "Press any key");
#else
    printf("%s", "Press any key");
#endif
    while (getchar() != '\n')
        ;
    getchar(); // wait for ENTER
    fflush(stdin);
}
