#include <stdio.h>

#include "perso.h"

#define SIZE 200

int tab[SIZE];

void dividers(int number)
{
	int shift = 1;
	for (int i = 1; i <= number/2; i++)
	{
		if (number % i == 0)
		{
			tab[i - shift] = i;
		}
		else
		{
			shift++;
		}
	}
#ifdef _WIN32
	printf_s("Divisors of %d : \n", number);
	for (int i = 0; i <= number/2 - 1; i++)
	{
		if (tab[i] == 0)
		{
			continue;
		}
		printf_s("| %d |\n", tab[i]);
	}
	printf_s("| %d |\n", number);
#else
	printf("Divisors of %d", number);
	for (int i = 0; i <= size - 1; i++)
	{
		printf("| %d |\n", tab[i]);
	}
	printf_s("| %d |\n", number);
#endif

}

int main(void)
{
	int number = inputi('N');
	dividers(number);
}