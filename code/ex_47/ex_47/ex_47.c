#include <stdbool.h>
#include <stdio.h>

#define SIZE 1000

int tab[SIZE];

void primes(void)
{
	bool valid = true;
	int shift = 0;
	for (int i = 1; i < SIZE; i++)
	{
		for (int j = 2; j <= i/2 + 1; i++)
		{
			valid = true;
			if ((i%j == 0) && (i != j))
			{
				valid = false;
				shift++;
			}
			if (valid == true)
			{
				tab[i - shift] = i;
				break;
			}
		}
	}

#ifdef _WIN32
	puts("Prime numbers : \n");
	for (int i = 0; i < SIZE; i++)
	{
		if (tab[i] == 0)
		{
			continue;
		}
		printf_s("| %d |\n", tab[i]);
	}
#else
	printf("Divisors of %d", number);
	for (int i = 0; i <= size - 1; i++)
	{
		printf("| %d |\n", tab[i]);
	}
	printf_s("| %d |\n", number);
#endif

}

int main(void)
{
	primes();
}