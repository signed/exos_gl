#include "entrelacement_bib.h"

/**************************************************************************************
	Fichier source : Entrelacement d'un nombre de d�part 
***************************************************************************************/

/*******************************************************************************************/

long extraire_chiffre ( long Nbre , long PositionChiffre )   /* extrait le chiffre de Nbre dont le rang est PositionChiffre */
	{
	/* d�finition des variables locales */
	long aux, i, chiffre ;
	
	aux = Nbre ;
	
	/* par divisions successives de aux, on place le chiffre voulu � l'extr�me droite : */
	for (i=0 ; i<PositionChiffre ; i++)   aux = aux / 10 ;	
	
	/* on r�cup�re alors le chiffre des unit�s = celui recherch� : */
	chiffre = aux % 10 ;

	return chiffre ;  
	}

/*******************************************************************************************/
	
